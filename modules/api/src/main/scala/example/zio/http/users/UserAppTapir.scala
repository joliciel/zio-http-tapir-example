package example.zio.http.users

import example.zio.http.HttpErrorProtocol
import sttp.model.StatusCode
import sttp.tapir.{AnyEndpoint, PublicEndpoint}
import sttp.tapir.generic.auto.*
import sttp.tapir.json.zio.*
import sttp.tapir.server.ziohttp.ZioHttpInterpreter
import sttp.tapir.ztapir.{endpoint as tapirEndpoint, *}
import zio.*
import zio.http.model.HttpError.*
import zio.http.model.{HttpError, Method, Status}
import zio.http.{endpoint, *}
import zio.json.*

import java.nio.charset.StandardCharsets

/**
 * An http app that: 
 *   - Accepts a `Request` and returns a `Response`
 *   - May fail with type of `Throwable`
 *   - Uses a `UserRepo` as the environment
 */
object UserAppTapir extends HttpErrorProtocol:
  extension[R, A] (c: RIO[R, A])
    def toInternalServerError: ZIO[R, HttpError, A] =
      c.mapError(throwable => InternalServerError(cause = Some(throwable)))

  def postUserLogic(user: User): ZIO[UserRepo, HttpError, String] =
    UserRepo.register(user)
      .toInternalServerError

  val postUserEndpoint: PublicEndpoint[User, HttpError, String, Any] =
    tapirEndpoint
      .errorOut(
        oneOf[HttpError](
          oneOfVariant[Conflict](StatusCode.Conflict, jsonBody[Conflict])
        )
      )
      .post
      .in("tapir")
      .in("users")
      .in(jsonBody[User].example(User("Sarah", 18)))
      .out(plainBody[String])

  val postUserHttp: Http[UserRepo, Throwable, Request, Response] =
    ZioHttpInterpreter().toHttp(postUserEndpoint.zServerLogic[UserRepo](postUserLogic))

  def getUserLogic(id: String): ZIO[UserRepo, HttpError, User] =
    UserRepo.lookup(id)
      .flatMap(ZIO.fromOption(_).orElseFail(NotFound(f"user $id not found")))
      .toInternalServerError

  val getUserEndpoint: PublicEndpoint[String, HttpError, User, Any] =
    tapirEndpoint
      .errorOut(
        oneOf[HttpError](
          oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound])
        )
      )
      .get
      .in("tapir")
      .in("users")
      .in(path[String]("id"))
      .out(jsonBody[User])

  val getUserHttp: Http[UserRepo, Throwable, Request, Response] =
    ZioHttpInterpreter().toHttp(getUserEndpoint.zServerLogic[UserRepo](getUserLogic))

  val getUsersLogic: ZIO[UserRepo, HttpError, List[User]] =
    UserRepo.users
      .toInternalServerError

  val getUsersEndpoint: PublicEndpoint[Unit, HttpError, List[User], Any] =
    tapirEndpoint
      .errorOut(
        oneOf[HttpError](
          oneOfVariant[NotFound](StatusCode.NotFound, jsonBody[NotFound])
        )
      )
      .in("tapir")
      .in("users")
      .get
      .out(jsonBody[List[User]])

  val getUsersHttp: Http[UserRepo, Throwable, Request, Response] =
    ZioHttpInterpreter().toHttp(getUsersEndpoint.zServerLogic[UserRepo](_ => getUsersLogic))

  val endpoints: List[AnyEndpoint] = List(
    postUserEndpoint,
    getUserEndpoint,
    getUsersEndpoint
  )

  def apply(): Http[UserRepo, Throwable, Request, Response] =
    postUserHttp ++ getUserHttp ++ getUsersHttp
