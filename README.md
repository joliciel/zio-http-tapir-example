# ZIO Http / Tapir example

This was initially copied from https://github.com/zio/zio-quickstart-restful-webservice

It was adapted to use Scala 3 and the latest versions of ZIO/Tapir, including:
- [ZIO HTTP](https://zio.dev/zio-http/) for the HTTP server
- [ZIO JSON](https://zio.dev/zio-json/) for the JSON serialization
- [ZIO Logging](https://zio.dev/zio-logging/) for integrate logging with slf4j
- [ZIO Config](https://zio.dev/zio-config/) for loading configuration data
- [Tapir](https://tapir.softwaremill.com/en/latest/) for endpoint definition and documentation

## Running The Example

First, open the console and clone the project using `git` (or you can simply download the project) and then change the directory:

```shell
git clone git@gitlab.com:joliciel/zio-http-tapir-example.git
cd zio-http-tapir-example
```

Once you are inside the project directory, run the application:

```shell
sbt
project api 
run
```

You can then navigate to the Swagger documentation as follows: http://localhost:3232/docs/
